<?php

namespace App\Controller;


use App\Services\Interfaces\GenericStoreServiceInterface;
use App\Services\StoreGeneratorService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

final class CitiesController extends AbstractController
{

    private GenericStoreServiceInterface $storeSolution;
    private string $defaultStore = 'CsvCityService';

    public function __invoke(
        Request $request,
        StoreGeneratorService $storeGeneratorService,
        RouterInterface $router
    ) : Response {
        $response = new Response();
        $response
            ->setPublic()
            ->setMaxAge(3600)
        ;
        $response->headers->addCacheControlDirective('no-cache');


        $queryString = '';
        if (!empty($request->getQueryString())) {
            $queryString = '?' . $request->getQueryString();
        }

        $trueUrl = $router->generate('cities', [], UrlGeneratorInterface::ABSOLUTE_URL) . $queryString;

        if ($trueUrl !== $request->getUri()) {
            return $this->redirect($trueUrl, Response::HTTP_MOVED_PERMANENTLY);
        }


        $this->storeSolution =  $storeGeneratorService->getStoreClass($this->defaultStore);

        $cities = $this->storeSolution->fetch((int)$request->get('departmentId'));
        $viewParameters = [
            'cities' => $cities
        ];


        return $this->render('cities.html.twig', $viewParameters, $response);
        
    }
}
