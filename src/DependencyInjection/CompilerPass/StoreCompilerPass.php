<?php
namespace App\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;
use App\Services\StoreGeneratorService;

class StoreCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(StoreGeneratorService::class)) {
            return;
        }

        $definition = $container->findDefinition(
            StoreGeneratorService::class
        );

        $taggedServices = $container->findTaggedServiceIds(
            'app.store_services'
        );

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall(
                'setStoreContainerService',
                array(new Reference($id))
            );
        }
    }
}