<?php
namespace  App\Repository;

abstract class AbstractRepository
{
    protected $lastModified;

    /**
     * @return \DateTimeImmutable
     */
    public function getLastModified(): \DateTimeImmutable
    {
        return $this->lastModified;
    }

    /**
     * Set last time modified
     *
     * @param string $filePath
     * @throws \RuntimeException
     * @return void
     */
    public function setLastModified(string $filePath): void
    {
        $lms = filemtime($filePath);
        if (!$lms) {
            throw new \RuntimeException('Could not read lastmod.');
        }
        $this->lastModified = \DateTimeImmutable::createFromFormat('U', $lms);
    }

}
