<?php

namespace App\Repository;

use App\Entity\City;

final class CityRepository extends AbstractRepository
{
    private array $dataByDepartmentId = [];

    public function __construct(string $filePath)
    {
        $handle = fopen($filePath, 'r');
        fgetcsv($handle); // Remove header
        while (($row = fgetcsv($handle)) !== false) {
            if (array_key_exists($row[1], $this->dataByDepartmentId) === false) {
                $this->dataByDepartmentId[$row[1]] = [];
            }
            $this->dataByDepartmentId[$row[1]][] = [
                'id' => $row[0],
                'name' => $row[3],
                'slug' => $row[2]
            ];
        }

        $this->setLastModified($filePath);
    }

    public function fetchByDepartmentId(int $departmentId, string $orderBy = 'ASC'): array
    {
        $cities = [];
        if (array_key_exists($departmentId, $this->dataByDepartmentId) === true) {
            $cities = [];

            if ($orderBy == 'ASC') {
                usort($this->dataByDepartmentId[$departmentId], function ($item1, $item2) {
                    return $item1['name'] <=> $item2['name'];
                });
            } else {
                usort($this->dataByDepartmentId[$departmentId], function ($item1, $item2) {
                    return $item2['name'] <=> $item1['name'];
                });
            }

            foreach ($this->dataByDepartmentId[$departmentId] as $cityData) {
                $city = new City();
                $city->setId($cityData['id']);
                $city->setName($cityData['name']);
                $cities[] = $city;
            }
        }

        return $cities;
    }

    public function getAll(): \Traversable
    {
        foreach($this->dataByDepartmentId as $departmentId => $cities) {
            foreach ($cities as $cityData) {
                $city = new City();
                $city
                    ->setId($cityData['id'])
                    ->setDepartmentId($departmentId)
                    ->setName($cityData['name'])
                    ->setSlug($cityData['slug'])
                ;
                yield $city;
            }
        }
    }

}
