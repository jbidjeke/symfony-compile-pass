<?php
namespace App\Services;

use App\Services\Interfaces\GenericStoreServiceInterface;

abstract class AbstractStoreService implements GenericStoreServiceInterface
{

    /**
     * @param string $labelClass
     * @return bool
     */
    public function isTypeMatch(string $labelClass): bool
    {
        return $labelClass === $this->getLabel();
    }

    /**
     * @return array 
     */
    abstract public function getLabel(): string;


    /**
    * @return array 
    */
    abstract public function fetch(int $id): array;
}