<?php
namespace App\Services;

use App\Repository\CityRepository;

class CsvCityService extends AbstractStoreService
{
    private CityRepository $cityRepository;

    public function __construct(CityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    /**
    * @return array 
    */
    public function fetch(int $id): array
    {
       return $this->cityRepository->fetchByDepartmentId($id); 
    }

    public function getLabel(): string
    {
        return 'CsvCityService';
    }
}