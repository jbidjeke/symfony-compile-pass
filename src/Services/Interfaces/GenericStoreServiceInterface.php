<?php

namespace App\Services\Interfaces;

interface GenericStoreServiceInterface {
    /**
     * @param string $labelClass
     * @return bool
     */
    public function isTypeMatch(string $labelClass): bool;

    /**
     * @return string LabelClass
     */
    public function getLabel(): string;


    /**
     * fetch store data by id
     * @return array 
     */
    public function fetch(int $id): array;
}