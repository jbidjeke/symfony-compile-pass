<?php
namespace App\Services;

use App\Repository\CitySQLiteRepository;

class SqliteCityService extends AbstractStoreService
{
    public function __construct(CitySQLiteRepository $citySqliteRepository){}

    /**
    * @return array 
    */
    public function fetch(int $id): array
    {
       return $this->citySqliteRepository->findCitiesByDepartmentId($id); 
    }

    public function getLabel(): string
    {
        return 'SqliteCityService';
    }
}