<?php
namespace App\Services;

use App\Services\Interfaces\GenericStoreServiceInterface;

class StoreGeneratorService 
{
    private array $storeContainerService;

    public function setStoreContainerService(GenericStoreServiceInterface $storeService): void
    {
        $this->storeContainerService[] = $storeService;
    }


    public function getStoreClass(string $storeClassLabel): GenericStoreServiceInterface
    {
        foreach ($this->storeContainerService as $storeService)
        {
            if ($storeService->isTypeMatch($storeClassLabel)) {
                return $storeService;
            }
        }

        throw new \Exception('None store service found for class ' . $storeClassLabel);
    }

}