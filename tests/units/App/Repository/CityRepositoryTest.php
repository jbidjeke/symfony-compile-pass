<?php

namespace units\App\Repository;

use App\Repository\CityRepository;
use PHPUnit\Framework\TestCase;
use Traversable;

class CityRepositoryTest extends TestCase
{
    private CityRepository $instance;

    public function setUp(): void
    {
        $this->instance = new CityRepository(dirname(__FILE__) . '/../../../../db/cities.csv');
    }

    public function testInstance()
    {
        $this->assertInstanceOf(
            CityRepository::class,
            $this->instance
        );
    }

    public function testFetchByDepartmentIdNotEmpty()
    {
        $this->assertNotEmpty(
            $this->instance->fetchByDepartmentId(70)
        );
    }

    public function testFetchByDepartmentWithAttributeKey()
    {
        $cities = $this->instance->fetchByDepartmentId(70);
        $this->assertTrue(property_exists($cities[0], 'name'));
    }

    public function testGetAllNotEmpty()
    {
        $this->assertNotEmpty(
            $this->instance->getAll()
        );
    }

    public function testGetAllResultType()
    {
        $this->assertInstanceOf(
            Traversable::class,
            $this->instance->getAll()
        );
    }
}